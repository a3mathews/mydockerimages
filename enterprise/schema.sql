-- This creates the database with initial database

create database phone;

use phone;

create table numbers (
  name varchar(50),
  number varchar(30)
);

insert into numbers values('Steve Shilling','555-1234');
insert into numbers values('Anne Mathews','645-783-8293');
insert into numbers values('Tom Ato','724-925-9482');
